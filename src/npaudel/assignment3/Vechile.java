package npaudel.assignment3;

public class Vechile {
	private String compCode;
	private String name;
	private int numWheel;
	private int rentPrice;
	
	// constructor
	
	Vechile(String compCode, String name, int numWheel, int rentPrice){
		setCompCode(compCode);
		setName(name);
		setNumWheel(numWheel);
		setRentPrice(rentPrice);
		
	}
	
	// getter and setter method
	
	public String getCompCode() {
		return compCode;
	}
	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumWheel() {
		return numWheel;
	}
	public void setNumWheel(int numWheel) {
		this.numWheel = numWheel;
	}
	public int getRentPrice() {
		return rentPrice;
	}
	public void setRentPrice(int rentPrice) {
		this.rentPrice = rentPrice;
	}
	
	public String toString() {
		return "\nCompany Code:=>        " + getCompCode() + "\nName of the Vechile:=> "+ getName()
		+ "\nNumber of Wheel:=>     "+ getNumWheel()+ "\nRental Price:=>        "   + getRentPrice();
				
	}
}
